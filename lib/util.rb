require './lib/pather'

SourceDir = 'src'
BuildDir = 'public'
Email = 'm@m-chrzan.xyz'

def src filename
  File.join SourceDir, filename
end

def build filename
  File.join BuildDir, filename
end

def path_to asset_id
  P.path_to asset_id
end

def git repo, path = 'about'
  "https://git.m-chrzan.xyz/#{repo}/#{path}"
end
