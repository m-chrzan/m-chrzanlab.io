def template
  ERB.new(File.read(src 'template.html.erb'), trim_mode: '-')
end

def write_templated_file content_filename, options
  content = File.read(src content_filename)
  write_templated content, content_filename, options
end

def write_erb erb_filename
  content = ERB.new(File.read(src erb_filename)).result
  cut_filename = erb_filename.sub /\.erb$/, ''
  File.write build(cut_filename), content
end

def write_templated_erb erb_filename, options
  content = ERB.new(File.read(src erb_filename), trim_mode: '-').result
  cut_filename = erb_filename.sub /\.erb$/, ''
  write_templated content, cut_filename, options
end

# Options should include
# head_title: used for <title> tag
# h1_title: used for <h1> on top of page
# title: used for both of the above, can be overridden with either
def write_templated content, filename, options
  head_title = options[:head_title] || options[:title] || ''
  h1_title = options[:h1_title] || options[:title] || ''
  File.write build(filename), template.result(binding)
end
