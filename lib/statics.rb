def write_statics statics
  statics = statics.each do | filename |
    source = src filename
    target = build filename
    FileUtils.mkdir_p(File.dirname target)
    FileUtils.cp_r source, target
  end
end
