require 'date'

def parse_post filename
  file_raw = File.read(src filename).split('---')
  post = YAML.load file_raw[0]

  post['date'] = DateTime.parse(post['date'])
  post['raw_content'] = file_raw[1].strip
  post['html_filename'] = filename.sub /\.erb$/, ''
  post['id'] = "blog_#{basename_no_extension filename}"
  post
end

def basename_no_extension filename
  File.basename filename.sub(/\.[\w.]*$/, '')
end

def compile_post post
  post['content'] = ERB.new(post['raw_content']).result binding
end

def compile_posts posts
  posts.each do |post|
    compile_post post
  end
end

def generate_blog_post post
  ERB.new(File.read(src 'post-template.html.erb')).result binding
end

@posts = nil

def posts
  @posts ||= Dir.new(src 'blog').children.map do |filename|
    post = parse_post "blog/#{filename}"
  end.sort do |a, b|
    b['date'] <=> a['date']
  end
end

def format_date date
  date.strftime '%B %-d, %Y'
end

def write_blog_files
  posts.each do |post|
    content = ERB.new(File.read(src 'post-template.html.erb')).result binding
    write_templated content, post['html_filename'], head_title: post['title'], h1_title: 'Blog.'
  end
end

def write_blog_archive
  write_templated_erb 'blog.html.erb', title: 'Blog.'
end
