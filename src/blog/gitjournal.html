title: Selfhosted Crossplatform Notes with Gitjournal
date: April 23, 2021 20:07
---
<h3>The problem</h3>
<p>
The "app market" for mobile phones is a very sad place. I wish it had followed
the direction of Linux distributions, with their repositories containing all the
useful software tools you'd ever need, free and open source. Instead we have a
duopoly of app stores that are both filled with "app developers" trying to make
a buck instead of tools that people actually want.
</p>

<p>
As such, when I find a phone application that is actually built for usefulness,
follows decent design principles, and allows for a personalized workflow, I
think it's necessary to give it a shoutout.
</p>

<p>
For a long time I've wanted a digital note taking solution that would allow me
to synchronize notes across multiple devices. I wouldn't want to use Evernote or
any other proprietary, centralized service. Selfhosting something like Nextcloud
just for the purpose of notes feels like shooting a fly with a cannon. If I were
limited to computers, it would be a no-brainer to just keep the notes in a git
repo (like I do with my <a href='<%= path_to "blog_cheatsheets" %>'>command line
cheatsheets</a>). How do we synchronize with a phone though?
</p>

<p>
Enter gitjournal.
</p>

<h3>Enter gitjournal</h3>
<p>
<a href='https://gitjournal.io/'>Gitjournal</a> essentially provides a git
client on your phone whose design is centered around note taking. I won't go
into all the features and design details, but it's got everything I need for
simple note taking:

<ul>
    <li>Edit notes in markdown</li>
    <li>Todo list mode</li>
    <li>Organize notes in a directory structure</li>
</ul>

And the killer feature is that you also provide a link and login credentials for
an external git repo. Every edit to the notes is committed and eventually pushed
to that repo. Any changes that appear in the repo will be pulled to your device.
</p>

<p>
And of course the git repository can be hosted anywhere. If you trust your
forge with your personal notes, be it GitHub, GitLab, sourcehut, or anywhere
else, your forge is now also your note synchronization hub. Personally, I host
my notes on my VPS.
</p>

<p>
Computer side, I've written a short script that takes care of git operations and
some of the metadata in gitjournal notes, and lets me select a note to edit with
<code>fzf</code>. But even manually navigating to notes and handling git
operations wouldn't be a bad experience.
</p>

<h3>Caveats</h3>
<p>
The app, in the end, is made for profit. In particular, on the Apple App Store
even the basic version costs money (it's free on Android). There are some
additional features that are further paywalled. This includes tagging and
multiple repositories, both of which would be nice to haves for me, but their
lack is not a deal breaker in my case. However, the app is open source (I think
including the "pro" features) and AGPL'd so if you know how to build it for your
phone, it's yours.
</p>

<p>
I haven't had to deal with merge conflicts in the app. I'm not sure how they're
handled. I'm not too worried though, if anything weird does happen, I'm sure
I'll be able to fix it computer-side. Nothing a little force pushing to master
can't solve.
</p>

<p>
You might have to be careful with some symbols in note titles that are treated
as special symbols by your OS (e.g. ~, !, [, ], etc.). The app won't complain
about them, they might cause problems on your computer.
</p>

<p>
As mentioned, the app commits every change you make to your notes. As such, I
prefer to use it for jotting down ideas, archiving links, writing down
interesting thoughts I have, long term todos  &mdash; in other words, more
persistent notes. For daily todo or shopping lists I use a different app. This
isn't a huge problem since I don't really need those things synchronized across
devices. I think in my ideal world, gitjournal would have "scratch notes" that
are not tracked in the git repo, so that I can keep all these things in one app
without littering git history with "buy eggs" every week.
</p>
