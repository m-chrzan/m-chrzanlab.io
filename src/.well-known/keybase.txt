==================================================================
https://keybase.io/mchrzan
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://m-chrzan.xyz
  * I am mchrzan (https://keybase.io/mchrzan) on keybase.
  * I have a public key ASCU2QEGp26ftNDw13KLSnJ3M_mNnPfTT2xgz4zdvC3jzAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01205e32aec1855ea5c79f7c3204112bc88ac19156e0595482b49a3247bb9c6fedd00a",
      "host": "keybase.io",
      "kid": "012094d90106a76e9fb4d0f0d7728b4a727733f98d9cf7d34f6c60cf8cddbc2de3cc0a",
      "uid": "55bc2bf5bb830ae09b3f00c5ddf25d19",
      "username": "mchrzan"
    },
    "merkle_root": {
      "ctime": 1590871034,
      "hash": "16b1ae935fee2c9075baf24359c11b0b5c3ef7379775979278d188787271f377dda4788ccb8e9223537ccfb67196a6521b3e5387980fdf476527d15e2f99798d",
      "hash_meta": "1bd710600ac3cf23264c016e785c0dcfe3f38bb0a2e7cd739eabe5a705305e99",
      "seqno": 16455118
    },
    "service": {
      "entropy": "2H1MHW8aHrTNhYmKNH3/HQeI",
      "hostname": "m-chrzan.xyz",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.5.1"
  },
  "ctime": 1590871109,
  "expire_in": 504576000,
  "prev": "1a73a8c9837c7f73ba13321cd82a77ebf31eb54cc28701fb7986fdf6c7840adc",
  "seqno": 20,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEglNkBBqdun7TQ8Ndyi0pydzP5jZz3009sYM+M3bwt48wKp3BheWxvYWTESpcCFMQgGnOoyYN8f3O6EzIc2Cp36/MetUzChwH7eYb99seECtzEIJt6Btud6ALWdYrOEW74DQPJzdfm5wNhQc9KKI6cb8QQAgHCo3NpZ8RAd1P8HtteDI7p7kqD0IEN0fsKVCc7AzKoJqiQ+PclxLDEXNXlMSEo9lLFc2dCASCw0LGg9N0XvF7cpSRxLKizB6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIOaF/5ShrNxBj0pjEUA0HTgLsnEedQeL6JLuL5uEnwzVo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/mchrzan

==================================================================
