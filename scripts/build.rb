#!/bin/ruby
require 'erb'
require 'fileutils'
require 'yaml'

require './lib/blog'
require './lib/statics'
require './lib/pather'
require './lib/templated'
require './lib/util'

def path_to asset_id
  P.path_to asset_id
end

if !Dir.exists? 'public'
  Dir.mkdir 'public'
end

P = Pather.new
P.cd 'src'

statics = [
  'style.css',
  '.well-known/acme-challenge/Tw4EQEgfzPP6garbkCV5pQJaJiOLhJYbuTbfPyFElcI',
  '.well-known/keybase.txt',
  'key.asc',
  'favicon.ico',
  'button.gif',
  'thesis.pdf',
  'thesis-slides.pdf'
]

P.add 'key', 'key.asc'
P.add 'style', 'style.css'
P.add 'button', 'button.gif'
P.add 'thesis', 'thesis.pdf'
P.add 'thesis-slides', 'thesis-slides.pdf'

P.add 'index', 'index.html'
P.add 'contact', 'contact.html'
P.add 'blog', 'blog.html'
P.add 'links', 'links.html'
P.add 'projects', 'projects.html'
P.add 'donate', 'donate.html'
P.add 'rss', 'rss.xml'
P.add 'sunday', 'sunday-corner'

if !Dir.exists? 'public/blog'
  Dir.mkdir 'public/blog'
end

posts.each do |post|
  P.add post['id'], post['html_filename']
end

write_statics statics
write_templated_erb 'index.html.erb', title: 'Martin Chrzanowski.'
write_templated_erb 'projects.html.erb', title: 'Projects.'
write_templated_erb 'contact.html.erb', title: 'Contact.'
write_templated_erb 'links.html.erb', title: 'Links.'
write_blog_archive
write_templated_file 'donate.html', title: 'Donations.'

P.cd 'blog'

compile_posts posts
write_blog_files

P.cd '..'

write_erb 'rss.xml.erb'
